package main

// missing: window title

import (
	"github.com/gotk3/gotk3/gtk"
)

type Album struct {
	ID          int
	Title       string
	Artist      string
	IsClassical bool
	Composer    string
}

func AlbumDataSet() []Album {
	return []Album{
		Album{1, "HQ", "Roy Harper", false, ""},
		Album{2, "The Rough Dancer and Cyclical Night", "Astor Piazzola", false, ""},
		Album{3, "The Black Light", "Calexico", false, ""},
		Album{4, "Symphony No.5", "CBSO", true, "Sibelius"},
	}
}

type AlbumPmod struct {
	data                []Album
	selectedAlbum       Album
	selectedAlbumNumber int
}

func NewAlbumPmod() *AlbumPmod {
	albums := AlbumDataSet()
	pmod := &AlbumPmod{
		data:                AlbumDataSet(),
		selectedAlbum:       albums[0],
		selectedAlbumNumber: 0,
	}
	return pmod
}

func (pmod *AlbumPmod) SelectedAlbum() *Album {
	return &pmod.selectedAlbum
}

func (pmod *AlbumPmod) SelectedAlbumID() int {
	return pmod.selectedAlbumNumber
}

func (pmod *AlbumPmod) SetSelectedAlbumID(id int) {
	if pmod.selectedAlbumNumber != id {
		pmod.Cancel()
		pmod.selectedAlbumNumber = id
		pmod.selectedAlbum = pmod.data[id]
	}
}

func (pmod *AlbumPmod) Title() string {
	return pmod.SelectedAlbum().Title
}

func (pmod *AlbumPmod) SetTitle(value string) {
	pmod.SelectedAlbum().Title = value
}

func (pmod *AlbumPmod) Artist() string {
	return pmod.SelectedAlbum().Artist
}

func (pmod *AlbumPmod) SetArtist(value string) {
	pmod.SelectedAlbum().Artist = value
}

func (pmod *AlbumPmod) IsClassical() bool {
	return pmod.SelectedAlbum().IsClassical
}

func (pmod *AlbumPmod) SetIsClassical(value bool) {
	pmod.SelectedAlbum().IsClassical = value
}

func (pmod *AlbumPmod) Composer() string {
	return pmod.SelectedAlbum().Composer
}

func (pmod *AlbumPmod) SetComposer(value string) {
	if pmod.IsClassical() {
		pmod.SelectedAlbum().Composer = value
	}
}

func (pmod *AlbumPmod) IsComposerFieldEnabled() bool {
	return pmod.IsClassical()
}

func (pmod *AlbumPmod) HasRowChanged() bool {
	return *pmod.SelectedAlbum() != pmod.data[pmod.selectedAlbumNumber]
}

func (pmod *AlbumPmod) IsApplyEnabled() bool {
	return pmod.HasRowChanged()
}

func (pmod *AlbumPmod) IsCancelEnabled() bool {
	return pmod.HasRowChanged()
}

func (pmod *AlbumPmod) Apply() {
	pmod.data[pmod.selectedAlbumNumber] = pmod.selectedAlbum
}

func (pmod *AlbumPmod) Cancel() {
	pmod.selectedAlbum = pmod.data[pmod.selectedAlbumNumber]
}

func (pmod *AlbumPmod) AlbumList() []string {
	var result []string
	for _, album := range pmod.data {
		result = append(result, album.Title)
	}
	return result
}

type AlbumForm struct {
	model         *AlbumPmod
	isLoadingView bool

	albumList *ListBox
	title     *gtk.Entry
	artist    *gtk.Entry
	classical *gtk.CheckButton
	composer  *gtk.Entry
	applyBtn  *gtk.Button
	cancelBtn *gtk.Button
	window    *gtk.Window
}

func NewAlbumForm(model *AlbumPmod) {
	form := AlbumForm{model: model}

	form.albumList = NewListBox(
		FilledWith(model.AlbumList()),
		OnSelectionChange(func() {
			form.model.SetSelectedAlbumID(form.albumList.Selection())
			form.loadFromPmod()
		}),
	)
	form.title = NewEntry(OnChange(func() {
		form.syncWithPmod()
	}))
	form.artist = NewEntry(OnChange(form.syncWithPmod))
	form.classical = NewCheckButton(Label("Classical"), OnToggle(form.syncWithPmod))
	form.composer = NewEntry(OnChange(form.syncWithPmod))
	form.applyBtn = NewButton(Label("Apply"), OnClick(func() {
		form.model.Apply()
		form.loadFromPmod()
	}))
	form.cancelBtn = NewButton(Label("Cancel"), OnClick(func() {
		form.model.Cancel()
		form.loadFromPmod()
	}))

	grid := NewGrid()
	grid.Attach(form.albumList.Unwrap(), Column(0), Row(0), ColSpan(3), RowSpan(6))
	grid.Attach(NewLabel("Title"), Column(3), Row(0))
	grid.Attach(form.title, Column(4), Row(0), ColSpan(3))
	grid.Attach(NewLabel("Artist"), Column(3), Row(1))
	grid.Attach(form.artist, Column(4), Row(1), ColSpan(3))
	grid.Attach(form.classical, Column(3), Row(2))
	grid.Attach(NewLabel("Composer"), Column(3), Row(3))
	grid.Attach(form.composer, Column(4), Row(3), ColSpan(3))
	grid.Attach(form.applyBtn, Column(4), Row(5))
	grid.Attach(form.cancelBtn, Column(5), Row(5))

	form.window = NewWindow(Title("TODO"))
	form.window.Add(grid.Unwrap())
	form.window.ShowAll()

	form.loadFromPmod() // Load the initial model.
}

func (form *AlbumForm) saveToPmod() {
	title, err := form.title.GetText()
	if err != nil {
		fail(err)
	}
	form.model.SetTitle(title)

	artist, err := form.artist.GetText()
	if err != nil {
		fail(err)
	}
	form.model.SetArtist(artist)

	form.model.SetIsClassical(form.classical.GetActive())

	composer, err := form.composer.GetText()
	if err != nil {
		fail(err)
	}
	form.model.SetComposer(composer)
}

func (form *AlbumForm) loadFromPmod() {
	form.unlessLoadingView(func() {
		form.albumList.Fill(form.model.AlbumList())
		form.albumList.SelectRow(form.model.SelectedAlbumID())

		form.title.SetText(form.model.Title())
		form.artist.SetText(form.model.Artist())
		form.classical.SetActive(form.model.IsClassical())
		form.composer.SetSensitive(form.model.IsComposerFieldEnabled())
		form.composer.SetText(form.model.Composer())
		form.applyBtn.SetSensitive(form.model.IsApplyEnabled())
		form.cancelBtn.SetSensitive(form.model.IsCancelEnabled())
	})
}

func (form *AlbumForm) syncWithPmod() {
	if form.isLoadingView {
		return
	}
	form.saveToPmod()
	form.loadFromPmod()
}

// Avoids infinite recursion when updating the view triggers an event, that
// updates the view again, triggering an event that…
func (form *AlbumForm) unlessLoadingView(body func()) {
	if form.isLoadingView {
		return
	}
	form.isLoadingView = true
	defer func() { form.isLoadingView = false }()
	body()
}

func main() {
	gtk.Init(nil)
	NewAlbumForm(NewAlbumPmod())
	gtk.Main()
}

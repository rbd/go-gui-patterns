package main

import (
	"fmt"
	"os"
	"unsafe"

	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

func NewEntry(options ...option) *gtk.Entry {
	entry, err := gtk.EntryNew()
	if err != nil {
		failf("entry: %s", err)
	}

	for _, opt := range options {
		opt(entry)
	}

	return entry
}

func NewCheckButton(options ...option) *gtk.CheckButton {
	btn, err := gtk.CheckButtonNew()
	if err != nil {
		failf("check button: %s", err)
	}

	for _, opt := range options {
		opt(btn)
	}

	return btn
}

func NewButton(options ...option) *gtk.Button {
	btn, err := gtk.ButtonNew()
	if err != nil {
		failf("button: %s", err)
	}

	for _, opt := range options {
		opt(btn)
	}

	return btn
}

// ListBox adapts *gtk.ListBox to our use.
type ListBox struct {
	delegate *gtk.ListBox
}

func NewListBox(options ...option) *ListBox {
	list, err := gtk.ListBoxNew()
	if err != nil {
		failf("list box: %s", err)
	}

	for _, opt := range options {
		opt(list)
	}

	return &ListBox{delegate: list}
}

func (list *ListBox) Selection() int {
	selected := list.delegate.GetSelectedRow()
	if selected != nil {
		return selected.GetIndex()
	}
	return 0
}

func (list *ListBox) SelectRow(id int) {
	if row := list.delegate.GetRowAtIndex(id); row != nil {
		list.delegate.SelectRow(row)
	}
}

func (list *ListBox) Unwrap() *gtk.ListBox {
	return list.delegate
}

func (list *ListBox) Fill(values []string) {
	// Because GetChildren is defined at the Container level, it returns a
	// list of *gtk.Widget instead of a list of *gtk.ListBoxRow. Since we
	// know the widget here is actually a row, it should safe to cast the
	// pointer.
	// The same reasonning works for the row child.
	list.delegate.GetChildren().Foreach(func(item interface{}) {
		row := (*gtk.ListBoxRow)(unsafe.Pointer(item.(*gtk.Widget)))
		child, err := row.GetChild()
		if err != nil {
			failf("row: get child: %s", err)
		}
		label := (*gtk.Label)(unsafe.Pointer(child))
		label.SetText(values[row.GetIndex()])
	})

}

func NewWindow(options ...option) *gtk.Window {
	window, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	if err != nil {
		failf("dialog: %s", err)
	}

	window.Connect("destroy", func() { gtk.MainQuit() })

	for _, opt := range options {
		opt(window)
	}

	return window
}

func NewLabel(str string) *gtk.Label {
	label, err := gtk.LabelNew(str)
	if err != nil {
		failf("label: %s", err)
	}

	return label
}

// Grid adapts *gtk.Grid to our use.
type Grid struct {
	delegate *gtk.Grid
}

func NewGrid(options ...option) *Grid {
	grid, err := gtk.GridNew()
	if err != nil {
		failf("grid: %s", err)
	}

	for _, opt := range options {
		opt(grid)
	}

	return &Grid{delegate: grid}
}

func (grid *Grid) Attach(child gtk.IWidget, options ...attachOption) {
	args := attachArgs{colspan: 1, rowspan: 1}
	for _, opt := range options {
		opt(&args)
	}
	grid.delegate.Attach(child, args.col, args.row, args.colspan, args.rowspan)
}

func (grid *Grid) Unwrap() *gtk.Grid {
	return grid.delegate
}

type option func(interface{})

func OnChange(cb func()) option {
	return func(widget interface{}) {
		widget.(connecter).Connect("changed", cb)
	}
}

func OnClick(cb func()) option {
	return func(widget interface{}) {
		widget.(connecter).Connect("clicked", cb)
	}
}

func OnToggle(cb func()) option {
	return func(widget interface{}) {
		widget.(connecter).Connect("toggled", cb)
	}
}

func OnSelectionChange(cb func()) option {
	return func(widget interface{}) {
		widget.(connecter).Connect("selected-rows-changed", cb)
	}
}

func Label(value string) option {
	return func(widget interface{}) {
		widget.(interface{ SetLabel(string) }).SetLabel(value)
	}
}

func FilledWith(values []string) option {
	return func(widget interface{}) {
		list := widget.(interface{ Add(gtk.IWidget) })
		for _, val := range values {
			list.Add(NewLabel(val))
		}
	}
}

func Title(value string) option {
	return func(widget interface{}) {
		widget.(interface{ SetTitle(string) }).SetTitle(value)
	}
}

type attachArgs struct {
	col, row, colspan, rowspan int
}

type attachOption func(*attachArgs)

func Column(value int) attachOption {
	return func(args *attachArgs) {
		args.col = value
	}
}

func Row(value int) attachOption {
	return func(args *attachArgs) {
		args.row = value
	}
}

func ColSpan(value int) attachOption {
	return func(args *attachArgs) {
		args.colspan = value
	}
}

func RowSpan(value int) attachOption {
	return func(args *attachArgs) {
		args.rowspan = value
	}
}

type connecter interface {
	// See https://pkg.go.dev/github.com/gotk3/gotk3/glib?tab=doc#Object.Connect
	Connect(string, interface{}, ...interface{}) (glib.SignalHandle, error)
}

func failf(pattern string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "Fatal error: "+pattern, args)
	os.Exit(-1)
}

func fail(err error) {
	failf("%s", err)
}
